import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppModuleShared } from './app.module.shared';
import { AppComponent } from './components/app/app.component';
import { CarsService } from './shared/cars.service/cars.service';
import { BrandsService } from './shared/brands.service/brands.service';
import { ModelsService } from './shared/models.service/models.service';
import { PhotoService } from './shared/photo.service/photo.service';
@NgModule({
    bootstrap: [ AppComponent ],
    imports: [
        BrowserModule,
        AppModuleShared
    ],
    providers: [
        { provide: 'BASE_URL', useFactory: getBaseUrl },
        CarsService,
        BrandsService,
        ModelsService,
        PhotoService
    ]
})
export class AppModule {
}

export function getBaseUrl() {
    return document.getElementsByTagName('base')[0].href;
}
