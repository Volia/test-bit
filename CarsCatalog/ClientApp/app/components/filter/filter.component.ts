﻿import { Component, Output, OnInit, Input, EventEmitter} from '@angular/core';

import { Model } from '../../entity/model';
import { Brand } from '../../entity/brand';

import { CarsFilterObject } from '../../entity/filters';
import { BrandsService } from '../../shared/brands.service/brands.service';

@Component({
    selector: 'filter-view',
    templateUrl: 'filter.component.html',
    styleUrls: ['filter.component.css'],
    providers: [BrandsService]
})
export class FilterComponent implements OnInit  {
    @Input() colors: string[];
    @Input() engineCapacities: number[];

    @Output() changeFilter = new EventEmitter();
    carsFilter: CarsFilterObject;
    priceFrom: number;
    priceTo: number;
    date: Date;
    brands: Brand[];
    constructor(private brandsService: BrandsService) {
        this.carsFilter = new CarsFilterObject();
        this.brands = [];
    }

    ngOnInit() {
        this.brandsService.getBrands().subscribe(brands => {
            this.brands = brands;
        })
    }
    onSelectBrand(brand: Brand): void {
        this.carsFilter.brand = this.carsFilter.brand == brand.id ? -1 : brand.id;
        this.carsFilter.model = -1;
        this.changeFilter.emit(this.carsFilter);
    }

    onSelectModel(model: Model): void {
        this.carsFilter.model = model.id;
        this.changeFilter.emit(this.carsFilter);
    }

    selectFilteringOption(option: any): void {
        if (typeof option === "string") {
            this.carsFilter.color = option;
        } else {
            this.carsFilter.engineCapacity = option;
        }
        this.changeFilter.emit(this.carsFilter);
    }

    onSelectPrice(): void {
        this.carsFilter.minPrice = (this.priceFrom == null || typeof this.priceFrom === 'undefined') ? 0 : this.priceFrom;
        this.carsFilter.maxPrice = (this.priceTo == null || typeof this.priceTo === 'undefined') ? 0 : this.priceTo;
        this.carsFilter.date = this.date;
        this.changeFilter.emit(this.carsFilter);
    }
}