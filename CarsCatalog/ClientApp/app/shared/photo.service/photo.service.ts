﻿import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { Photo } from '../../entity/photo';

@Injectable()
export class PhotoService {
    private apiUrl = 'api/Photo';

    constructor(private http: Http) { }

    createPhoto(photo: Photo): Observable<Photo> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers });

        return this.http.post(this.apiUrl, JSON.stringify(photo), options)
            .map(res => res.json() as Photo)
            .catch(this.handleError);
    }

    private handleError(error: any) {
        console.error('Error: ', error);
        return Observable.throw(error.message || error);
    }
}