﻿using AutoMapper;
using CarsCatalog.BLL.DTO;
using CarsCatalog.DAL.Entities;

namespace CarsCatalog.web.App_Start
{
    public static class MapperConfig
    {
        public static void ConfigureMapping()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Car, CarDTO>();
                cfg.CreateMap<Model, ModelDTO>();
                cfg.CreateMap<Brand, BrandDTO>();
                cfg.CreateMap<Price, PriceDTO>();
                cfg.CreateMap<Photo, PhotoDTO>();
                cfg.CreateMap<CarDTO, Car>();
                cfg.CreateMap<ModelDTO, Model>();
                cfg.CreateMap<BrandDTO, Brand>();
                cfg.CreateMap<PriceDTO, Price>();
                cfg.CreateMap<PhotoDTO, Photo>();
            });
        }
    }
}