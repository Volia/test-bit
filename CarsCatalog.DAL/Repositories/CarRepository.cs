﻿using CarsCatalog.DAL.EF;
using CarsCatalog.DAL.Entities;
using CarsCatalog.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace CarsCatalog.DAL.Repositories
{
    public class CarRepository : IRepository<Car>
    {
        private CarsCatalogContext db;

        public CarRepository(CarsCatalogContext context)
        {
            this.db = context;
        }

        public IQueryable<Car> GetAll()
        {
            return db.Cars
                .Include(c => c.Model)
                .Include(c => c.Brand)
                .Include(c => c.Prices)
                .Include(c => c.Photos);
        }

        public Car Get(int id)
        {
            return db.Cars
                .Include(c => c.Model)
                .Include(c => c.Brand)
                .Include(c => c.Prices)
                .Include(c => c.Photos)
                .FirstOrDefault(e => e.Id == id);
        }

        public Car Create(Car car)
        {
            Car newCar = new Car {
                BrandId = car.BrandId,
                ModelId = car.ModelId,
                Color = car.Color,
                Description = car.Description,
                EngineCapacity = car.EngineCapacity
            };

            db.Cars.Add(newCar);
            db.SaveChanges();

            var photos = car.Photos.ToList();
            var prices = car.Prices.ToList();

            for (int i = 0; i < photos.Count; i++)
            {
                db.Photos.Add(new Photo {
                    CarId = newCar.Id,
                    Name = photos[i].Name
                });
            }
            for (int i = 0; i < prices.Count; i++)
            {
                db.Prices.Add(new Price
                {
                    CarId = newCar.Id,
                    Date = prices[i].Date,
                    Sum = prices[i].Sum
                });
            }
            db.SaveChanges();
            return car;
        }

        public void Update(Car car)
        {
            db.Entry(car).State = EntityState.Modified;
            db.SaveChanges();
        }

        public IQueryable<Car> Find(Func<Car, Boolean> predicate)
        {
            return db.Cars.Where(c => predicate(c));
        }

        public void Delete(int id)
        {
            Car car = db.Cars.Include(c => c.Prices).Include(c => c.Photos).Where(c => c.Id == id).FirstOrDefault();
            if (car != null)
            {
                db.Prices.RemoveRange(car.Prices);
                db.Photos.RemoveRange(car.Photos);
                db.Cars.Remove(car);
                db.SaveChanges();
            }
        }
    }
}

