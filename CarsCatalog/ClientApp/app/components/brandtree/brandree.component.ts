﻿import { Component, Output, Input, EventEmitter } from '@angular/core';
import { Brand } from '../../entity/brand';
import { Model } from '../../entity/model';
import { BrandsService } from '../../shared/brands.service/brands.service';

@Component({
    selector: 'brand-tree',
    templateUrl: './brandtree.component.html',
    styleUrls: ['./brandtree.component.css']
})
export class BrandtreeComponent {
    @Output() onSelectBrand = new EventEmitter();
    @Output() onSelectModel = new EventEmitter();
    @Input() brands: Brand[];

    constructor(private brandService: BrandsService) {
        this.brands = [];
    }

    onChangeBrand(brand: Brand): void {
           this.onSelectBrand.emit(brand);
    }

    onChangeModel(model: Model): void {
        this.onSelectModel.emit(model);
    }
}
