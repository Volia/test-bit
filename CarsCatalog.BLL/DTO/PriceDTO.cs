﻿using System;

namespace CarsCatalog.BLL.DTO
{
    public class PriceDTO
    {
        //public int Id { get; set; }
        public decimal Sum { get; set; }
        public DateTime Date { get; set; }
        public int CarId { get; set; }
    }
}
