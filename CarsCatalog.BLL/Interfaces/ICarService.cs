﻿using CarsCatalog.BLL.DTO;
using System.Collections.Generic;

namespace CarsCatalog.BLL.Interfaces
{
    public interface ICarService
    {
        CarDTO AddCar(CarDTO carDto);
        void Update(CarDTO carDto);
        IEnumerable<CarDTO> GetCars();
        CarDTO GetCar(int? id);
        void Delete(int id);
        void Dispose();
    }
}
