﻿using AutoMapper;
using CarsCatalog.BLL.DTO;
using CarsCatalog.BLL.Interfaces;
using CarsCatalog.DAL.Entities;
using CarsCatalog.DAL.Interfaces;
using System.Collections.Generic;

namespace CarsCatalog.BLL.Services
{
    public class CarService : ICarService
    {
        IUnitOfWork Database { get; set; }

        public CarService(IUnitOfWork uow)
        {
            this.Database = uow;
        }

        public CarDTO AddCar(CarDTO carDto)
        {
            Car car = Mapper.Map<CarDTO, Car>(carDto);
            return Mapper.Map<Car, CarDTO>(Database.Cars.Create(car));
        }

        public CarDTO GetCar(int? id)
        {
            var car = Database.Cars.Get(id.Value);
            return Mapper.Map<Car, CarDTO>(car);
        }

        public IEnumerable<CarDTO> GetCars()
        {
            var cars = Database.Cars.GetAll();
            List<CarDTO> carDtos = new List<CarDTO>();
            foreach (var car in cars)
            {
                carDtos.Add(Mapper.Map<Car, CarDTO>(car));
            }
            return carDtos;
        }

        public void Update(CarDTO carDto)
        {
            Car car = Mapper.Map<CarDTO, Car>(carDto);
            Database.Cars.Update(car);
        }

        public void Delete(int id)
        {
            Database.Cars.Delete(id);
        }

        public void Dispose()
        {
            Database.Dispose();
        }
    }
}
