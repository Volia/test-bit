﻿import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Car } from '../../entity/car';
import { Price } from '../../entity/price';

@Component({
    selector: 'car-view',
    templateUrl: 'carview.component.html',
    styleUrls: ['carview.component.css']
})
export class CarViewComponent implements OnInit {
    @Input() car: Car;
    @Output() onDelete = new EventEmitter();
    partPhotos: string;
    photo: string;
    carPrice: string;
    carDescription: string;
    constructor() {
        this.partPhotos = 'assets/photos/'
    }

    ngOnInit() {
        let price: number = this.getPrice();
        this.carPrice = this.formatPrice(price);
        this.carDescription = this.formatDescription(this.car.description);
        this.photo = this.car.photos[0] ? this.car.photos[0].name : 'noimage.png';
    }

    getPrice(): number {
        return this.car.prices.sort((a: Price, b: Price) => {
            return +new Date(a.date) - +new Date(b.date);
        })[this.car.prices.length - 1].sum;
    }

    formatPrice(n : number): string {
       return '$ ' + n.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
    }

    formatDescription(des: string) {
        if (des.length > 25) {
            des = des.substr(0, 22) + '...';
        } else if (des.length == 0) {
            des = 'No description.'
        }
        return des;
    }

    deleteCar(car: Car) {
        this.onDelete.emit(car);
    }
}