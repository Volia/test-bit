import { Component, Input, OnInit } from '@angular/core';
import { Car } from '../../../entity/car';
import { Price } from '../../../entity/price';
@Component({
    selector: 'view-info',
    templateUrl: './viewinfo.component.html',
    styleUrls: ['./viewinfo.component.css']
})
export class ViewinfoComponent implements OnInit {
    @Input() car: Car;
    carPrice: string;
    constructor() {
        this.car = new Car();
        this.carPrice = '';
    }

    ngOnInit() {
        let price: number = this.getPrice();
        this.carPrice = this.formatPrice(price);
    }

    getPrice(): number {
        return this.car.prices.sort((a: Price, b: Price) => {
            return +new Date(a.date) - +new Date(b.date);
        })[this.car.prices.length - 1].sum;
    }

    formatPrice(n: number): string {
        return '$ ' + n.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
    }

    formatDate(d: Date): string {
        var date = new Date(d);
        return date.getDate() + '.' + date.getMonth() + '.' + date.getFullYear();
    }
}
