﻿import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { Price } from '../../entity/price';

@Injectable()
export class PricesService {
    private apiUrl = 'api/Price';

    constructor(private http: Http) { }

    createPrice(price: Price): Observable<Price> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers });

        return this.http.post(this.apiUrl, JSON.stringify(price), options)
            .map(res => res.json() as Price)
            .catch(this.handleError);
    }

    private handleError(error: any) {
        console.error('Error: ', error);
        return Observable.throw(error.message || error);
    }
}