import { Component, Output, Input, EventEmitter } from '@angular/core';

import { Brand } from '../../entity/brand';

@Component({
    selector: 'add-brand',
    templateUrl: './addbrand.component.html',
    styleUrls: ['./addbrand.component.css']
})
export class AddBrandComponent {
    @Output() onCreate = new EventEmitter();
    @Input() brand: Brand;
    constructor() {
    }

    onSubmit() {
        this.onCreate.emit(this.brand);
    }
}
