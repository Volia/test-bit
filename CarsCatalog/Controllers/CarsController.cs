﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using CarsCatalog.BLL.Interfaces;
using CarsCatalog.BLL.DTO;
using System.IO;
using Microsoft.AspNetCore.Hosting;

namespace CarsCatalog.Controllers
{
    [Produces("application/json")]
    [Route("api/Cars")]
    public class CarsController : Controller
    {
        private readonly ICarService carService;
        private readonly IHostingEnvironment _appEnvironment;
        public CarsController(ICarService serv, IHostingEnvironment appEnvironment)
        {
            carService = serv;
            _appEnvironment = appEnvironment;
        }

        // GET: api/Cars
        [HttpGet]
        public IEnumerable<CarDTO> Get()
        {
            var cars = carService.GetCars();
            return cars;
        }

        // GET: api/Cars/5
        [HttpGet("{id}", Name = "GetCar")]
        public CarDTO Get(int id)
        {
            var car = this.carService.GetCar(id);
            return car;
        }

        // POST: api/Cars
        [HttpPost]
        public CarDTO Post([FromBody]CarDTO carDto)
        {
            var car = this.carService.AddCar(carDto);
            return car;
        }

        // PUT: api/Cars/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]CarDTO carDto)
        {
            this.carService.Update(carDto);
        }
        
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            var photos = this.carService.GetCar(id).Photos;
            this.carService.Delete(id);
            foreach (var photo in photos)
            {
                var file = _appEnvironment.WebRootPath +  "/assets/photos/" + photo.Name;
                System.IO.File.Delete(file);
            }
        }
    }
}
