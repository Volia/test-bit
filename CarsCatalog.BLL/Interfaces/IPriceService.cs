﻿using CarsCatalog.BLL.DTO;
using System.Collections.Generic;

namespace CarsCatalog.BLL.Interfaces
{
    public interface IPriceService
    {
        PriceDTO AddPrice(PriceDTO priceDto);
        void Dispose();
    }
}
