﻿using CarsCatalog.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CarsCatalog.DAL.EF
{
    public static class DbInitializer
    {
        public static void Initialize(CarsCatalogContext context)
        {
            context.Database.EnsureCreated();
            if (context.Cars.Any())
            {
                return;   // DB has been seeded
            }

            //var brands = new Brand[]
            //    {
            //    new Brand { Id = 1, Name = "Mersedes-Benz" },
            //    new Brand { Id = 2, Name = "Audi" },
            //    new Brand { Id = 3, Name = "Volkswagen" },
            //    new Brand { Id = 4, Name = "Honda" },
            //    new Brand { Id = 5, Name = "Toyota" },
            //    new Brand { Id = 6, Name = "Subaru" },
            //    new Brand { Id = 7, Name = "BMW" },
            //    new Brand { Id = 8, Name = "Mazda" },
            //};

            //foreach (Brand b in brands)
            //{
            //    context.Brands.Add(b);
            //}
            //context.SaveChanges();

            //var models = new Model[]
            //{
            //    new Model { Id = 1, Name = "GL 550", BrandId = 1 },
            //    new Model { Id = 2, Name = "S-Class", BrandId = 1 },
            //    new Model { Id = 3, Name = "A6", BrandId = 2 },
            //    new Model { Id = 4, Name = "Amarok Hlghline", BrandId = 3 },
            //    new Model { Id = 5, Name = "Civic 5D", BrandId = 4 },
            //    new Model { Id = 6, Name = "Camry", BrandId = 5 },
            //    new Model { Id = 7, Name = "Forester", BrandId = 6 },
            //    new Model { Id = 8, Name = "X5", BrandId = 7 },
            //    new Model { Id = 9, Name = "6 2.0i Touring+", BrandId = 8 },
            //    new Model { Id = 10, Name = "6 2.5 Sport", BrandId = 8 }
            //};

            //foreach (Model m in models)
            //{
            //    context.Models.Add(m);
            //}

            //context.SaveChanges();

            //var cars = new Car[]
            //{
            //    new Car { Id = 1, BrandId = 1, Color = "White", EngineCapacity = 5.5F, ModelId = 1, Description = "Гаражное хранение, Сервисная книжка, Не бит, Не крашен" },
            //    new Car { Id = 2, BrandId = 2, Color = "Black", EngineCapacity = 2.0F, ModelId = 3, Description = "Центральный замок, Подушка безопасности (Airbag), ABS, Иммобилайзер, Сигнализация, ABD, ESP, Галогенные фары, Серворуль" },
            //    new Car { Id = 3, BrandId = 3, Color = "Black", EngineCapacity = 2.0F, ModelId = 4, Description = "Центральный замок, Подушка безопасности (Airbag), ABS, Иммобилайзер, Сигнализация, ABD, ESP, Галогенные фары, Серворуль, Бронированный автомобиль" },
            //    new Car { Id = 4, BrandId = 4, Color = "Black", EngineCapacity = 1.8F, ModelId = 5, Description = "Центральный замок, Подушка безопасности (Airbag), ABS, Иммобилайзер, Сигнализация, ESP, Серворуль" },
            //    new Car { Id = 5, BrandId = 5, Color = "Silver", EngineCapacity = 2.4F, ModelId = 6, Description = "Центральный замок, Подушка безопасности (Airbag), ABS, Иммобилайзер, Галогенные фары, Замок на КПП" },
            //    new Car { Id = 6, BrandId = 1, Color = "Silver", EngineCapacity = 3.2F, ModelId = 2, Description = "ГБО-4 поколения, обслуженная, требует мелкой косметики, хозяин по ТП" },
            //    new Car { Id = 7, BrandId = 6, Color = "Black", EngineCapacity = 2.0F, ModelId = 7, Description = "Центральный замок, Подушка безопасности (Airbag), ABS" },
            //    new Car { Id = 8, BrandId = 7, Color = "White", EngineCapacity = 2.0F, ModelId = 8, Description = "Гаражное хранение, Первый владелец, Не бит, Не крашен, Индивидуальная комплектация" },
            //    new Car { Id = 9, BrandId = 8, Color = "Red", EngineCapacity = 2.0F, ModelId = 9, Description = "Центральный замок, Подушка безопасности (Airbag), ABS, Иммобилайзер, Сигнализация, ESP, Замок на КПП, Серворуль" },
            //    new Car { Id = 10, BrandId = 8, Color = "Black", EngineCapacity = 2.5F, ModelId = 10, Description = "Усилитель руля, Кожаный салон, Эл. стеклоподъемники, Бортовой компьютер, Климат контроль, Круиз контроль, Парктроник, Подогрев сидений, Сенсор дождя, Датчик света, Омыватель фар, Подогрев зеркал, Электропакет, Мультируль, Запуск кнопкой" }
            //};

            //foreach (Car c in cars)
            //{
            //    context.Cars.Add(c);
            //}

            //context.SaveChanges();

            //var photos = new Photo[]
            //{
            //    new Photo { Id = 1, Name = "mercedes-benz_gl-550__1.jpg", CarId = 1 },
            //    new Photo { Id = 2, Name = "mercedes-benz_gl-550__2.jpg", CarId = 1 },
            //    new Photo { Id = 3, Name = "mercedes-benz_gl-550__3.jpg", CarId = 1 },
            //    new Photo { Id = 4, Name = "audi_a6__1.jpg", CarId = 2 },
            //    new Photo { Id = 5, Name = "audi_a6__2.jpg", CarId = 2 },
            //    new Photo { Id = 6, Name = "audi_a6__3.jpg", CarId = 2 },
            //    new Photo { Id = 7, Name = "volkswagen_amarok__1.jpg", CarId = 3 },
            //    new Photo { Id = 8, Name = "volkswagen_amarok__2.jpg", CarId = 3 },
            //    new Photo { Id = 9, Name = "volkswagen_amarok__3.jpg", CarId = 3 },
            //    new Photo { Id = 10, Name = "honda_civic__1.jpg", CarId = 4 },
            //    new Photo { Id = 11, Name = "honda_civic__2.jpg", CarId = 4 },
            //    new Photo { Id = 12, Name = "honda_civic__3.jpg", CarId = 4 },
            //    new Photo { Id = 13, Name = "toyota_camry__1.jpg", CarId = 5 },
            //    new Photo { Id = 14, Name = "toyota_camry__2.jpg", CarId = 5 },
            //    new Photo { Id = 15, Name = "toyota_camry__3.jpg", CarId = 5 },
            //    new Photo { Id = 16, Name = "mercedes-benz_s-class__1.jpg", CarId = 6 },
            //    new Photo { Id = 17, Name = "mercedes-benz_s-class__2.jpg", CarId = 6 },
            //    new Photo { Id = 18, Name = "mercedes-benz_s-class__3.jpg", CarId = 6 },
            //    new Photo { Id = 19, Name = "subaru_forester__1.jpg", CarId = 7 },
            //    new Photo { Id = 20, Name = "subaru_forester__2.jpg", CarId = 7 },
            //    new Photo { Id = 21, Name = "subaru_forester__3.jpg", CarId = 7 },
            //    new Photo { Id = 22, Name = "bmw_x5__1.jpg", CarId = 8 },
            //    new Photo { Id = 23, Name = "bmw_x5__2.jpg", CarId = 8 },
            //    new Photo { Id = 24, Name = "bmw_x5__3.jpg", CarId = 8 },
            //    new Photo { Id = 25, Name = "mazda_6__1.jpg", CarId = 9 },
            //    new Photo { Id = 26, Name = "mazda_6__2.jpg", CarId = 9 },
            //    new Photo { Id = 27, Name = "mazda_6__3.jpg", CarId = 9 },
            //    new Photo { Id = 28, Name = "mazda_6_Sport_1.jpg", CarId = 10 },
            //    new Photo { Id = 29, Name = "mazda_6_Sport_2.jpg", CarId = 10 },
            //    new Photo { Id = 30, Name = "mazda_6_Sport_3.jpg", CarId = 10 }
            //};

            //foreach (Photo p in photos)
            //{
            //    context.Photos.Add(p);
            //}
            //context.SaveChanges();

            //var prices = new Price[]
            //{
            //    new Price { Id = 1, Sum = 49999, Date = new DateTime(2017, 10, 5), CarId = 1 },
            //    new Price { Id = 2, Sum = 40999, Date = new DateTime(2016, 10, 5), CarId = 1 },
            //    new Price { Id = 3, Sum = 39999, Date = new DateTime(2015, 10, 5), CarId = 1 },
            //    new Price { Id = 4, Sum = 27900, Date = new DateTime(2017, 10, 5), CarId = 2 },
            //    new Price { Id = 5, Sum = 26900, Date = new DateTime(2016, 10, 5), CarId = 2 },
            //    new Price { Id = 6, Sum = 25900, Date = new DateTime(2015, 10, 5), CarId = 2 },
            //    new Price { Id = 7, Sum = 21999, Date = new DateTime(2017, 10, 5), CarId = 3 },
            //    new Price { Id = 8, Sum = 20999, Date = new DateTime(2016, 10, 5), CarId = 3 },
            //    new Price { Id = 9, Sum = 19999, Date = new DateTime(2015, 10, 5), CarId = 3 },
            //    new Price { Id = 10, Sum = 9550, Date = new DateTime(2017, 10, 5), CarId = 4 },
            //    new Price { Id = 11, Sum = 8550, Date = new DateTime(2016, 10, 5), CarId = 4 },
            //    new Price { Id = 12, Sum = 7550, Date = new DateTime(2015, 10, 5), CarId = 4 },
            //    new Price { Id = 13, Sum = 9600, Date = new DateTime(2017, 10, 5), CarId = 5 },
            //    new Price { Id = 14, Sum = 8600, Date = new DateTime(2016, 10, 5), CarId = 5 },
            //    new Price { Id = 15, Sum = 7600, Date = new DateTime(2015, 10, 5), CarId = 5 },
            //    new Price { Id = 16, Sum = 7500, Date = new DateTime(2017, 10, 5), CarId = 6 },
            //    new Price { Id = 17, Sum = 6500, Date = new DateTime(2016, 10, 5), CarId = 6 },
            //    new Price { Id = 18, Sum = 5500, Date = new DateTime(2015, 10, 5), CarId = 6 },
            //    new Price { Id = 19, Sum = 9700, Date = new DateTime(2017, 10, 5), CarId = 7 },
            //    new Price { Id = 20, Sum = 8700, Date = new DateTime(2016, 10, 5), CarId = 7 },
            //    new Price { Id = 21, Sum = 7700, Date = new DateTime(2015, 10, 5), CarId = 7 },
            //    new Price { Id = 22, Sum = 48900, Date = new DateTime(2017, 10, 5), CarId = 8 },
            //    new Price { Id = 23, Sum = 47900, Date = new DateTime(2016, 10, 5), CarId = 8 },
            //    new Price { Id = 24, Sum = 46900, Date = new DateTime(2015, 10, 5), CarId = 8 },
            //    new Price { Id = 25, Sum = 14500, Date = new DateTime(2017, 10, 5), CarId = 9 },
            //    new Price { Id = 26, Sum = 13500, Date = new DateTime(2016, 10, 5), CarId = 9 },
            //    new Price { Id = 27, Sum = 12500, Date = new DateTime(2015, 10, 5), CarId = 9 },
            //    new Price { Id = 28, Sum = 12600, Date = new DateTime(2017, 10, 5), CarId = 10 },
            //    new Price { Id = 29, Sum = 11600, Date = new DateTime(2016, 10, 5), CarId = 10 },
            //    new Price { Id = 30, Sum = 10600, Date = new DateTime(2015, 10, 5), CarId = 10 }
            //};

            //foreach (Price p in prices)
            //{
            //    context.Prices.Add(p);
            //}

            var brands = new Brand[]
                {
                new Brand { Name = "Mersedes-Benz" },
                new Brand { Name = "Audi" },
                new Brand { Name = "Volkswagen" },
                new Brand { Name = "Honda" },
                new Brand { Name = "Toyota" },
                new Brand { Name = "Subaru" },
                new Brand { Name = "BMW" },
                new Brand { Name = "Mazda" },
            };

            foreach (Brand b in brands)
            {
                context.Brands.Add(b);
            }
            context.SaveChanges();

            var models = new Model[]
            {
                new Model { Name = "GL 550", BrandId = 1 },
                new Model { Name = "S-Class", BrandId = 1 },
                new Model { Name = "A6", BrandId = 2 },
                new Model { Name = "Amarok Hlghline", BrandId = 3 },
                new Model { Name = "Civic 5D", BrandId = 4 },
                new Model { Name = "Camry", BrandId = 5 },
                new Model { Name = "Forester", BrandId = 6 },
                new Model { Name = "X5", BrandId = 7 },
                new Model { Name = "6 2.0i Touring+", BrandId = 8 },
                new Model { Name = "6 2.5 Sport", BrandId = 8 }
            };

            foreach (Model m in models)
            {
                context.Models.Add(m);
            }

            context.SaveChanges();

            var cars = new Car[]
            {
                new Car { BrandId = 1, Color = "White", EngineCapacity = 5.5F, ModelId = 1, Description = "Гаражное хранение, Сервисная книжка, Не бит, Не крашен" },
                new Car { BrandId = 2, Color = "Black", EngineCapacity = 2.0F, ModelId = 3, Description = "Центральный замок, Подушка безопасности (Airbag), ABS, Иммобилайзер, Сигнализация, ABD, ESP, Галогенные фары, Серворуль" },
                new Car { BrandId = 3, Color = "Black", EngineCapacity = 2.0F, ModelId = 4, Description = "Центральный замок, Подушка безопасности (Airbag), ABS, Иммобилайзер, Сигнализация, ABD, ESP, Галогенные фары, Серворуль, Бронированный автомобиль" },
                new Car { BrandId = 4, Color = "Black", EngineCapacity = 1.8F, ModelId = 5, Description = "Центральный замок, Подушка безопасности (Airbag), ABS, Иммобилайзер, Сигнализация, ESP, Серворуль" },
                new Car { BrandId = 5, Color = "Silver", EngineCapacity = 2.4F, ModelId = 6, Description = "Центральный замок, Подушка безопасности (Airbag), ABS, Иммобилайзер, Галогенные фары, Замок на КПП" },
                new Car { BrandId = 1, Color = "Silver", EngineCapacity = 3.2F, ModelId = 2, Description = "ГБО-4 поколения, обслуженная, требует мелкой косметики, хозяин по ТП" },
                new Car { BrandId = 6, Color = "Black", EngineCapacity = 2.0F, ModelId = 7, Description = "Центральный замок, Подушка безопасности (Airbag), ABS" },
                new Car { BrandId = 7, Color = "White", EngineCapacity = 2.0F, ModelId = 8, Description = "Гаражное хранение, Первый владелец, Не бит, Не крашен, Индивидуальная комплектация" },
                new Car { BrandId = 8, Color = "Red", EngineCapacity = 2.0F, ModelId = 9, Description = "Центральный замок, Подушка безопасности (Airbag), ABS, Иммобилайзер, Сигнализация, ESP, Замок на КПП, Серворуль" },
                new Car { BrandId = 8, Color = "Black", EngineCapacity = 2.5F, ModelId = 10, Description = "Усилитель руля, Кожаный салон, Эл. стеклоподъемники, Бортовой компьютер, Климат контроль, Круиз контроль, Парктроник, Подогрев сидений, Сенсор дождя, Датчик света, Омыватель фар, Подогрев зеркал, Электропакет, Мультируль, Запуск кнопкой" }
            };

            foreach (Car c in cars)
            {
                context.Cars.Add(c);
            }

            context.SaveChanges();

            var photos = new Photo[]
            {
                new Photo { Name = "mercedes-benz_gl-550__1.jpg", CarId = 1 },
                new Photo { Name = "mercedes-benz_gl-550__2.jpg", CarId = 1 },
                new Photo { Name = "mercedes-benz_gl-550__3.jpg", CarId = 1 },
                new Photo { Name = "audi_a6__1.jpg", CarId = 2 },
                new Photo { Name = "audi_a6__2.jpg", CarId = 2 },
                new Photo { Name = "audi_a6__3.jpg", CarId = 2 },
                new Photo { Name = "volkswagen_amarok__1.jpg", CarId = 3 },
                new Photo { Name = "volkswagen_amarok__2.jpg", CarId = 3 },
                new Photo { Name = "volkswagen_amarok__3.jpg", CarId = 3 },
                new Photo { Name = "honda_civic__1.jpg", CarId = 4 },
                new Photo { Name = "honda_civic__2.jpg", CarId = 4 },
                new Photo { Name = "honda_civic__3.jpg", CarId = 4 },
                new Photo { Name = "toyota_camry__1.jpg", CarId = 5 },
                new Photo { Name = "toyota_camry__2.jpg", CarId = 5 },
                new Photo { Name = "toyota_camry__3.jpg", CarId = 5 },
                new Photo { Name = "mercedes-benz_s-class__1.jpg", CarId = 6 },
                new Photo { Name = "mercedes-benz_s-class__2.jpg", CarId = 6 },
                new Photo { Name = "mercedes-benz_s-class__3.jpg", CarId = 6 },
                new Photo { Name = "subaru_forester__1.jpg", CarId = 7 },
                new Photo { Name = "subaru_forester__2.jpg", CarId = 7 },
                new Photo { Name = "subaru_forester__3.jpg", CarId = 7 },
                new Photo { Name = "bmw_x5__1.jpg", CarId = 8 },
                new Photo { Name = "bmw_x5__2.jpg", CarId = 8 },
                new Photo { Name = "bmw_x5__3.jpg", CarId = 8 },
                new Photo { Name = "mazda_6__1.jpg", CarId = 9 },
                new Photo { Name = "mazda_6__2.jpg", CarId = 9 },
                new Photo { Name = "mazda_6__3.jpg", CarId = 9 },
                new Photo { Name = "mazda_6_Sport_1.jpg", CarId = 10 },
                new Photo { Name = "mazda_6_Sport_2.jpg", CarId = 10 },
                new Photo { Name = "mazda_6_Sport_3.jpg", CarId = 10 }
            };

            foreach (Photo p in photos)
            {
                context.Photos.Add(p);
            }
            context.SaveChanges();

            var prices = new Price[]
            {
                new Price { Sum = 49999, Date = new DateTime(2017, 10, 5), CarId = 1 },
                new Price { Sum = 40999, Date = new DateTime(2016, 10, 5), CarId = 1 },
                new Price { Sum = 39999, Date = new DateTime(2015, 10, 5), CarId = 1 },
                new Price { Sum = 27900, Date = new DateTime(2017, 10, 5), CarId = 2 },
                new Price { Sum = 26900, Date = new DateTime(2016, 10, 5), CarId = 2 },
                new Price { Sum = 25900, Date = new DateTime(2015, 10, 5), CarId = 2 },
                new Price { Sum = 21999, Date = new DateTime(2017, 10, 5), CarId = 3 },
                new Price { Sum = 20999, Date = new DateTime(2016, 10, 5), CarId = 3 },
                new Price { Sum = 19999, Date = new DateTime(2015, 10, 5), CarId = 3 },
                new Price { Sum = 9550, Date = new DateTime(2017, 10, 5), CarId = 4 },
                new Price { Sum = 8550, Date = new DateTime(2016, 10, 5), CarId = 4 },
                new Price { Sum = 7550, Date = new DateTime(2015, 10, 5), CarId = 4 },
                new Price { Sum = 9600, Date = new DateTime(2017, 10, 5), CarId = 5 },
                new Price { Sum = 8600, Date = new DateTime(2016, 10, 5), CarId = 5 },
                new Price { Sum = 7600, Date = new DateTime(2015, 10, 5), CarId = 5 },
                new Price { Sum = 7500, Date = new DateTime(2017, 10, 5), CarId = 6 },
                new Price { Sum = 6500, Date = new DateTime(2016, 10, 5), CarId = 6 },
                new Price { Sum = 5500, Date = new DateTime(2015, 10, 5), CarId = 6 },
                new Price { Sum = 9700, Date = new DateTime(2017, 10, 5), CarId = 7 },
                new Price { Sum = 8700, Date = new DateTime(2016, 10, 5), CarId = 7 },
                new Price { Sum = 7700, Date = new DateTime(2015, 10, 5), CarId = 7 },
                new Price { Sum = 48900, Date = new DateTime(2017, 10, 5), CarId = 8 },
                new Price { Sum = 47900, Date = new DateTime(2016, 10, 5), CarId = 8 },
                new Price { Sum = 46900, Date = new DateTime(2015, 10, 5), CarId = 8 },
                new Price { Sum = 14500, Date = new DateTime(2017, 10, 5), CarId = 9 },
                new Price { Sum = 13500, Date = new DateTime(2016, 10, 5), CarId = 9 },
                new Price { Sum = 12500, Date = new DateTime(2015, 10, 5), CarId = 9 },
                new Price { Sum = 12600, Date = new DateTime(2017, 10, 5), CarId = 10 },
                new Price { Sum = 11600, Date = new DateTime(2016, 10, 5), CarId = 10 },
                new Price { Sum = 10600, Date = new DateTime(2015, 10, 5), CarId = 10 }
            };

            foreach (Price p in prices)
            {
                context.Prices.Add(p);
            }
            context.SaveChanges();

            
        }
    }
}
