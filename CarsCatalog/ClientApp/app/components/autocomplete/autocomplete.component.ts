﻿import { Component, Input,Output, ElementRef, EventEmitter } from "@angular/core";

@Component({
  selector: "autocomplete",
  templateUrl: './autocomplete.component.html',
  styleUrls: ['./autocomplete.component.css'],
})
export class AutocompleteComponent<T> {
    query : T;
    @Input() data: T[];
    @Input() placeholder: string;
    @Input() step: string;
    @Input() type: string;
    @Output() selectFilteringOption = new EventEmitter();
    filteredData : T[];
    elementRef: ElementRef;

    constructor(myElement: ElementRef) {
        this.elementRef = myElement;
        this.filteredData = [];
    }

    filter() {
        if (this.query) {
            let queryStr = typeof this.query === 'string' ? this.query.toLowerCase() : this.query;
            this.filteredData = this.data.filter(function (el : any) {
                return el.toString().toLowerCase().indexOf(queryStr) > -1;
            }.bind(this));
        } else {
            this.filteredData = [];
            let defVal = this.type == 'text' ? '' : -1;
            this.selectFilteringOption.emit(defVal);
        }
    }

    select(item : T) {
        this.query = item;
        this.filteredData = [];
        this.selectFilteringOption.emit(item);
    }
}