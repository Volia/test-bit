﻿using System.Collections.Generic;

namespace CarsCatalog.BLL.DTO
{
    public class BrandDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<ModelDTO> Models { get; set; }
    }
}
