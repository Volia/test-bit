﻿import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { Car } from '../../entity/car';

@Injectable()
export class CarsService {
    private apiUrl = 'api/Cars';

    constructor(private http: Http) { }

    getCars(): Observable<Car[]> {
        return this.http.get(this.apiUrl)
            .map(res => res.json() as Car[])
            .catch(this.handleError);
    }

    getCar(id: number): Observable<Car> {
        let url = `${this.apiUrl}/${id}`;
        return this.http.get(url)
            .map(res => res.json() as Car)
            .catch(this.handleError);
    }

    deleteCar(car: Car) {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers });
        let url = `${this.apiUrl}/${car.id}`;
        return this.http.delete(url, options).catch(this.handleError);

    }

    createCar(car: Car): Observable<Car> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers });

        return this.http.post(this.apiUrl, JSON.stringify(car), options)
            .map(res => res.json() as Car)
            .catch(this.handleError);
    }

    updateCar(car: Car): Observable<Car> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers });
        let url = `${this.apiUrl}/${car.id}`;
        return this.http.put(url, JSON.stringify(car), options).catch(this.handleError);
    }

    private handleError(error: any) {
        console.error('Error: ', error);
        return Observable.throw(error.message || error);
    }
}