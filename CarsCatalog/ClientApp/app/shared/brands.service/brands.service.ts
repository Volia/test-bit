﻿import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { Brand } from '../../entity/brand';

@Injectable()
export class BrandsService {
    private apiUrl = 'api/Brand';

    constructor(private http: Http) { }

    getBrands(): Observable<Brand[]> {
        return this.http.get(this.apiUrl)
            .map(res => res.json() as Brand[])
            .catch(this.handleError);
    }

    deleteBrand(brand: Brand) {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers });
        let url = `${this.apiUrl}/${brand.id}`;
        return this.http.delete(url, options).catch(this.handleError);

    }

    createBrand(brand: Brand): Observable<Brand> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers });

        return this.http.post(this.apiUrl, JSON.stringify(brand), options)
            .map(res => res.json() as Brand)
            .catch(this.handleError);
    }

    updateBrand(brand: Brand): Observable<Brand> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers });
        let url = `${this.apiUrl}/${brand.id}`;
        return this.http.put(url, JSON.stringify(brand), options)
            .catch(this.handleError);
    }
    private handleError(error: any) {
        console.error('Error: ', error);
        return Observable.throw(error.message || error);
    }
}