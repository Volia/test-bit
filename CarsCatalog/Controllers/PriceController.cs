﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using CarsCatalog.BLL.DTO;
using CarsCatalog.BLL.Interfaces;

namespace CarsCatalog.Controllers
{
    [Produces("application/json")]
    [Route("api/Price")]
    public class PriceController : Controller
    {
        private readonly IPriceService priceService;

        public PriceController(IPriceService serv)
        {
            priceService = serv;
        }

        // POST: api/Price
        [HttpPost]
        public PriceDTO Post([FromBody]PriceDTO priceDto)
        {
            var price = this.priceService.AddPrice(priceDto);
            return price;
        }
        
        
    }
}
