import { Component, OnInit } from '@angular/core';
import { Car } from '../../entity/car';
import { CarsService } from '../../shared/cars.service/cars.service';
import { CarsFilterObject } from '../../entity/filters';
import { CarsFilterPipe } from '../../shared/filters/cars.pipe';

@Component({
    selector: 'home',
    templateUrl: './home.component.html',
    styleUrls: ['home.component.css'],
    providers: [ CarsService ]
})
export class HomeComponent implements OnInit {
    cars: Car[];
    colors: string[];
    engineCapacities: number[];

    filterObj: CarsFilterObject;
    constructor(private carService: CarsService) {
        this.cars = [];
        this.colors = [];
        this.engineCapacities = [];
        this.filterObj = new CarsFilterObject();
    }

    ngOnInit() {
        this.carService
            .getCars()
            .subscribe(cars => {
                this.cars = cars;
                this.initAllColors();
                this.initEngineCapacities();
            });
        
    }

    changeFilter(filterObj: CarsFilterObject): void {
        this.filterObj = filterObj;
    }

    initAllColors(): void {
        for (var i = 0; i < this.cars.length; i++) {
            if (this.colors.indexOf(this.cars[i].color) == -1) {
                this.colors.push(this.cars[i].color)
            }
        }
    }

    initEngineCapacities(): void {
        for (var i = 0; i < this.cars.length; i++) {
            if (this.engineCapacities.indexOf(this.cars[i].engineCapacity) == -1) {
                this.engineCapacities.push(this.cars[i].engineCapacity)
            }
        }
    }

    onDelete(car: Car): void {
        this.carService.deleteCar(car).subscribe(res => {
            let index = this.cars.indexOf(car);
            if (index != -1) {
                this.cars.splice(index, 1);
            }
        });
    }
}
