import { Component, Output, Input, EventEmitter } from '@angular/core';

import { Model } from '../../entity/model';
import { Brand } from '../../entity/brand';
@Component({
    selector: 'add-model',
    templateUrl: './addmodel.component.html',
    styleUrls: ['./addmodel.component.css']
})
export class AddModelComponent {
    @Output() onCreate = new EventEmitter();
    @Output() onDelete = new EventEmitter();
    @Input() model: Model;
    @Input() brands: Brand[];
    @Input() brand: Brand;
    constructor() {
    }

    onSubmit() {
        this.onCreate.emit(this.model);
    }

    onDel() {
        this.onDelete.emit(this.model);
    }
}
