﻿using CarsCatalog.DAL.EF;
using CarsCatalog.DAL.Entities;
using CarsCatalog.DAL.Interfaces;
using System;
using System.Linq;

namespace CarsCatalog.DAL.Repositories
{
    public class PriceRepository : IRepository<Price>
    {
        private CarsCatalogContext db;

        public PriceRepository(CarsCatalogContext context)
        {
            this.db = context;
        }

        public IQueryable<Price> GetAll()
        {
            return db.Prices;
        }

        public Price Get(int id)
        {
            return db.Prices.Find(id);
        }

        public Price Create(Price price)
        {
            db.Prices.Add(price);
            db.SaveChanges();
            return price;
        }

        public void Update(Price price)
        {
            //db.Entry(price).State = EntityState.Modified;
        }

        public IQueryable<Price> Find(Func<Price, Boolean> predicate)
        {
            return db.Prices.Where(c => predicate(c));
        }

        public void Delete(int id)
        {
            Price price = db.Prices.Find(id);
            if (price != null)
                db.Prices.Remove(price);
        }
    }
}
