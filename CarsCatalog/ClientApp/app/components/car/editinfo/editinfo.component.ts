import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { Car } from '../../../entity/car';
import { Brand } from '../../../entity/brand';
import { Price } from '../../../entity/price';
import { Photo } from '../../../entity/photo';
import { PhotoService } from '../../../shared/photo.service/photo.service';

export class FileHolder {
    public serverResponse: any;
    public pending: boolean = false;
    constructor(public src: string, public file: File) { }
}

@Component({
    selector: 'edit-info',
    templateUrl: './editinfo.component.html',
    styleUrls: ['./editinfo.component.css'],
    providers: [PhotoService]
})
export class EditinfoComponent implements OnInit {
    @Input() car: Car;
    @Output() onEditCar = new EventEmitter();
    currentPrice: number;
    lastPrice: number;
    constructor(private photoService: PhotoService) { }

    ngOnInit() {
        this.currentPrice = this.getPrice();
        this.lastPrice = this.currentPrice;
    }

    getPrice(): number {
        if (this.car.id) {
            return this.car.prices.sort((a: Price, b: Price) => {
                return +new Date(a.date) - +new Date(b.date);
            })[this.car.prices.length - 1].sum;
        }
        return 0;
    }

    onSubmit() {
        this.onEditCar.emit({ car: this.car, current: this.currentPrice, last: this.lastPrice });
    }

    onUploadFinished(file: FileHolder) {
        let carId = 0;
        let name = JSON.parse(file.serverResponse._body).name;
        if (this.car.id) {
            carId = this.car.id;
            this.photoService.createPhoto(new Photo(name = name, carId = carId)).subscribe(photo => {
                this.car.photos.push(photo);
            });
        } else {
            this.car.photos.push(new Photo(name = name, carId = carId));
        }
        
        
    }
}
