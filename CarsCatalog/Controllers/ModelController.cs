﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using CarsCatalog.BLL.Interfaces;
using CarsCatalog.BLL.DTO;

namespace CarsCatalog.Controllers
{
    [Produces("application/json")]
    [Route("api/Model")]
    public class ModelController : Controller
    {
        private readonly IModelService modelService;

        public ModelController(IModelService serv)
        {
            modelService = serv;
        }

        // GET: api/Model
        [HttpGet]
        public IEnumerable<ModelDTO> Get()
        {
            var models = this.modelService.GetModels();
            return models;
        }

        // GET: api/Model/5
        [HttpGet("{id}", Name = "GetModel")]
        public ModelDTO Get(int id)
        {
            return this.modelService.GetModel(id);
        }

        // POST: api/Model [FromBody]string value
        [HttpPost]
        public ModelDTO Post([FromBody]ModelDTO modelDto)
        {
            var model = this.modelService.AddModel(modelDto);
            return model;
        }

        // PUT: api/Brand/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]ModelDTO modelDto)
        {
            this.modelService.Update(modelDto);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            this.modelService.Delete(id);
        }
    }
}
