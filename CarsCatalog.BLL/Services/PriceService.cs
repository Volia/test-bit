﻿using AutoMapper;
using CarsCatalog.BLL.DTO;
using CarsCatalog.BLL.Interfaces;
using CarsCatalog.DAL.Entities;
using CarsCatalog.DAL.Interfaces;
using System.Collections.Generic;

namespace CarsCatalog.BLL.Services
{
    public class PriceService : IPriceService
    {
        IUnitOfWork Database { get; set; }

        public PriceService(IUnitOfWork uow)
        {
            this.Database = uow;
        }

        public PriceDTO AddPrice(PriceDTO priceDto)
        {
            Price price = Mapper.Map<PriceDTO, Price>(priceDto);
            return Mapper.Map<Price, PriceDTO>(Database.Prices.Create(price));
        }
        
        public void Dispose()
        {
            Database.Dispose();
        }
    }
}
