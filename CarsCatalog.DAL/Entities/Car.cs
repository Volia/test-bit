﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CarsCatalog.DAL.Entities
{
    public class Car
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int? ModelId { get; set; }
        public virtual Model Model { get; set; }

        public int? BrandId { get; set; }
        public virtual Brand Brand { get; set; }

        public string Color { get; set; }
        public float EngineCapacity { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Price> Prices { get; set; }
        public virtual ICollection<Photo> Photos { get; set; }
    }
}
