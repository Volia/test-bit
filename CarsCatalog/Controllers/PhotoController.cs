﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using CarsCatalog.BLL.Interfaces;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using CarsCatalog.BLL.DTO;

namespace CarsCatalog.Controllers
{
    [Produces("application/json")]
    [Route("api/Photo")]
    public class PhotoController : Controller
    {
        private readonly IPhotoService photoService;
        private readonly IHostingEnvironment _appEnvironment;

        public PhotoController(IPhotoService serv, IHostingEnvironment appEnvironment)
        {
            photoService = serv;
            _appEnvironment = appEnvironment;
        }

        [HttpPost("UploadFiles")]
        public async Task<IActionResult> Post(IFormFile image)
        {
            var name = string.Format(@"{0}" + Path.GetExtension(image.FileName), Guid.NewGuid());
            var filePath = "/assets/photos/" + name;

            using (var fileStream = new FileStream(_appEnvironment.WebRootPath + filePath, FileMode.Create))
            {
                await image.CopyToAsync(fileStream);
            }
            return Ok(new { name });
        }

        // POST: api/Photo
        [HttpPost]
        public PhotoDTO Post([FromBody]PhotoDTO photoDto)
        {
            var photo = this.photoService.AddPhoto(photoDto);
            return photo;
        }
    }
}
