﻿using CarsCatalog.DAL.Entities;
using Microsoft.EntityFrameworkCore;



namespace CarsCatalog.DAL.EF
{
    public class CarsCatalogContext : DbContext
    {
        public DbSet<Car> Cars { get; set; }
        public DbSet<Brand> Brands { get; set; }
        public DbSet<Model> Models { get; set; }
        public DbSet<Photo> Photos { get; set; }
        public DbSet<Price> Prices { get; set; }

        public CarsCatalogContext(DbContextOptions<CarsCatalogContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Car>().ToTable("Car");
            modelBuilder.Entity<Brand>().ToTable("Brand");
            modelBuilder.Entity<Model>().ToTable("Model");
            modelBuilder.Entity<Photo>().ToTable("Photo");
            modelBuilder.Entity<Price>().ToTable("Price");
        }
    }
}
