using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SpaServices.Webpack;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using CarsCatalog.web.App_Start;
using CarsCatalog.BLL.Interfaces;
using CarsCatalog.BLL.Services;
using CarsCatalog.DAL.EF;
using Microsoft.EntityFrameworkCore;
using CarsCatalog.DAL.Interfaces;
using CarsCatalog.DAL.Repositories;

namespace CarsCatalog
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            string connection = Configuration.GetConnectionString("CarCatalogConnection");
            services.AddDbContext<CarsCatalogContext>(options => options.UseSqlServer(connection));
            services.AddMvc();
            services.AddTransient<ICarService, CarService>();
            services.AddTransient<IBrandService, BrandService>();
            services.AddTransient<IModelService, ModelService>();
            services.AddTransient<IPriceService, PriceService>();
            services.AddTransient<IPhotoService, PhotoService>();
            services.AddTransient<IUnitOfWork, EFUnitOfWork>();
            MapperConfig.ConfigureMapping();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseWebpackDevMiddleware(new WebpackDevMiddlewareOptions
                {
                    HotModuleReplacement = true
                });
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");

                routes.MapSpaFallbackRoute(
                    name: "spa-fallback",
                    defaults: new { controller = "Home", action = "Index" });
            });
        }
    }
}
