﻿namespace CarsCatalog.BLL.DTO
{
    public class ModelDTO
    {
        public int Id { get; set; }
        public int BrandId { get; set; }
        public string Name { get; set; }
    }
}
