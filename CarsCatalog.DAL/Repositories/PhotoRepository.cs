﻿using CarsCatalog.DAL.EF;
using CarsCatalog.DAL.Entities;
using CarsCatalog.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace CarsCatalog.DAL.Repositories
{
    public class PhotoRepository : IRepository<Photo>
    {
        private CarsCatalogContext db;

        public PhotoRepository(CarsCatalogContext context)
        {
            this.db = context;
        }

        public IQueryable<Photo> GetAll()
        {
            return db.Photos;
        }

        public Photo Get(int id)
        {
            return db.Photos.Find(id);
        }

        public Photo Create(Photo photo)
        {
            db.Photos.Add(photo);
            db.SaveChanges();
            return photo;
        }

        public void Update(Photo photo)
        {
            db.Entry(photo).State = EntityState.Modified;
            db.SaveChanges();
        }

        public IQueryable<Photo> Find(Func<Photo, Boolean> predicate)
        {
            return db.Photos.Where(c => predicate(c));
        }

        public void Delete(int id)
        {
            Photo photo = db.Photos.Find(id);
            if (photo != null)
                db.Photos.Remove(photo);
        }
    }
}
