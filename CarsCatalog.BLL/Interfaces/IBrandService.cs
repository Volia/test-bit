﻿using CarsCatalog.BLL.DTO;
using System.Collections.Generic;

namespace CarsCatalog.BLL.Interfaces
{
    public interface IBrandService
    {
        BrandDTO AddBrand(BrandDTO brandDto);
        IEnumerable<BrandDTO> GetBrands();
        BrandDTO GetBrand(int? id);
        void Update(BrandDTO brandDto);
        void Delete(int id);
        void Dispose();
    }
}
