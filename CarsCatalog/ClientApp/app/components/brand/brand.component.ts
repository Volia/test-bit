﻿import { Component, OnInit } from '@angular/core';
import { BrandsService } from '../../shared/brands.service/brands.service';

import { Brand } from '../../entity/brand';

@Component({
    selector: 'brand',
    templateUrl: './brand.component.html',
    styleUrls: ['./brand.component.css'],
    providers: [BrandsService]
})
export class BrandComponent implements OnInit {
    brands: Brand[];
    brand: Brand;
    constructor(private brandService: BrandsService) {
        this.brands = [];
        this.brand = new Brand();
    }

    ngOnInit() {
        this.brandService.getBrands().subscribe(brands => {
            this.brands = brands;
        });
    }

    onDelete(brand: Brand): void {
        this.brandService.deleteBrand(brand).subscribe(res => {
            let index = this.brands.indexOf(brand);

            if (index > -1) {
                this.brands.splice(index, 1);
            }
        })
    }

    onCreate(brand: Brand) {
        let brandName = brand.name;
        if (!brand.id) {
            this.brandService.createBrand(brand).subscribe(res => {
                this.brands.push(res);
            });
        } else {
            this.brandService.updateBrand(brand).subscribe(res => {
                this.brands.filter(b => b.id == brand.id)[0].name = brandName;
            });
        }
    }

    selectBrand(brand: Brand): void {
        this.brand = brand;
    }
}
