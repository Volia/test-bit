﻿namespace CarsCatalog.BLL.DTO
{
    public class PhotoDTO
    {
        public int Id { get; set; }
        public int CarId { get; set; }
        public string Name { get; set; }
    }
}
