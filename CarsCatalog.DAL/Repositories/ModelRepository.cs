﻿using CarsCatalog.DAL.EF;
using CarsCatalog.DAL.Entities;
using CarsCatalog.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace CarsCatalog.DAL.Repositories
{
    public class ModelRepository : IRepository<Model>
    {
        private CarsCatalogContext db;

        public ModelRepository(CarsCatalogContext context)
        {
            this.db = context;
        }

        public IQueryable<Model> GetAll()
        {
            return db.Models;
        }

        public Model Get(int id)
        {
            return db.Models.Find(id);
        }

        public Model Create(Model model)
        {
            db.Models.Add(model);
            db.SaveChanges();
            return model;
        }

        public void Update(Model models)
        {
            db.Entry(models).State = EntityState.Modified;
            db.SaveChanges();
        }

        public IQueryable<Model> Find(Func<Model, Boolean> predicate)
        {
            return db.Models.Where(c => predicate(c));
        }

        public void Delete(int id)
        {
            Model model = db.Models.Include(m => m.Cars).Where(m => m.Id == id).FirstOrDefault();
            if (model != null) {
                model.Cars.ToList().ForEach(c =>
                {
                    var car = db.Cars.Include(a => a.Photos).Include(a => a.Prices).Where(a => a.Id == c.Id).FirstOrDefault();
                    db.Prices.RemoveRange(car.Prices);
                    db.Photos.RemoveRange(car.Photos);
                    db.Cars.Remove(c);
                });
                db.Models.Remove(model);
                db.SaveChanges();
            }
        }
    }
}
