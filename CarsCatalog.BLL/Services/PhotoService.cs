﻿using AutoMapper;
using CarsCatalog.BLL.DTO;
using CarsCatalog.BLL.Interfaces;
using CarsCatalog.DAL.Entities;
using CarsCatalog.DAL.Interfaces;
using System.Collections.Generic;

namespace CarsCatalog.BLL.Services
{
    public class PhotoService : IPhotoService
    {
        IUnitOfWork Database { get; set; }

        public PhotoService(IUnitOfWork uow)
        {
            this.Database = uow;
        }

        public PhotoDTO AddPhoto(PhotoDTO photoDto)
        {
            Photo photo = Mapper.Map<PhotoDTO, Photo>(photoDto);
            return Mapper.Map<Photo, PhotoDTO>(Database.Photos.Create(photo));
        }
        
        public void Dispose()
        {
            Database.Dispose();
        }
    }
}
