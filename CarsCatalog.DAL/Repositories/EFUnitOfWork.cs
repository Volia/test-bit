﻿using CarsCatalog.DAL.EF;
using CarsCatalog.DAL.Entities;
using CarsCatalog.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarsCatalog.DAL.Repositories
{
    public class EFUnitOfWork : IUnitOfWork
    {
        private CarsCatalogContext db;
        private BrandRepository brandRepository;
        private CarRepository carRepository;
        private ModelRepository modelRepository;
        private PhotoRepository photoRepository;
        private PriceRepository priceRepository;

        public EFUnitOfWork(CarsCatalogContext context)
        {
            db = context;
        }

        public IRepository<Brand> Brands
        {
            get
            {
                if (this.brandRepository == null)
                {
                    this.brandRepository = new BrandRepository(db);
                }
                return this.brandRepository;
            }
        }

        public IRepository<Car> Cars
        {
            get
            {
                if (this.carRepository == null)
                {
                    this.carRepository = new CarRepository(db);
                }
                return this.carRepository;
            }
        }

        public IRepository<Model> Models
        {
            get
            {
                if (this.modelRepository == null)
                {
                    this.modelRepository = new ModelRepository(db);
                }
                return this.modelRepository;
            }
        }

        public IRepository<Photo> Photos
        {
            get
            {
                if (this.photoRepository == null)
                {
                    this.photoRepository = new PhotoRepository(db);
                }
                return this.photoRepository;
            }
        }

        public IRepository<Price> Prices
        {
            get
            {
                if (this.priceRepository == null)
                {
                    this.priceRepository = new PriceRepository(db);
                }
                return this.priceRepository;
            }
        }

        public void Save()
        {
            db.SaveChanges();
        }

        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
                this.disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
