﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using CarsCatalog.BLL.Interfaces;
using CarsCatalog.BLL.DTO;

namespace CarsCatalog.Controllers
{
    [Produces("application/json")]
    [Route("api/Brand")]
    public class BrandController : Controller
    {
        private readonly IBrandService brandService;

        public BrandController(IBrandService serv)
        {
            brandService = serv;
        }

        // GET: api/Brand
        [HttpGet]
        public IEnumerable<BrandDTO> Get()
        {
            var brands = this.brandService.GetBrands();
            return brands;
        }

        // GET: api/Brand/5
        [HttpGet("{id}", Name = "GetBrand")]
        public BrandDTO Get(int id)
        {
            return this.brandService.GetBrand(id);
        }

        // POST: api/Brand [FromBody]string value
        [HttpPost]
        public BrandDTO Post([FromBody]BrandDTO brandDto)
        {
            var br = this.brandService.AddBrand(brandDto);
            return br;
        }
        
        // PUT: api/Brand/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]BrandDTO brandDto)
        {
            this.brandService.Update(brandDto);
        }
        
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            this.brandService.Delete(id);
        }
    }
}
