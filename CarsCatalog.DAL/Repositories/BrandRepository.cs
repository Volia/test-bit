﻿using CarsCatalog.DAL.EF;
using CarsCatalog.DAL.Entities;
using CarsCatalog.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CarsCatalog.DAL.Repositories
{
    public class BrandRepository : IRepository<Brand>
    {
        private CarsCatalogContext db;

        public BrandRepository(CarsCatalogContext context)
        {
            this.db = context;
        }

        public IQueryable<Brand> GetAll()
        {
            return db.Brands.Include(c => c.Models);
        }

        public Brand Get(int id)
        {
            return db.Brands.Find(id);
        }

        public Brand Create(Brand brand)
        {
            db.Brands.Add(brand);
            db.SaveChanges();
            return brand;
        }

        public void Update(Brand brand)
        {
            db.Entry(brand).State = EntityState.Modified;
            db.SaveChanges();
        }

        public IQueryable<Brand> Find(Func<Brand, Boolean> predicate)
        {
            return db.Brands.Include(c => c.Models).Where(c => predicate(c));
        }

        public void Delete(int id)
        {
            Brand brand = db.Brands.Include(b => b.Cars).Include(b => b.Models).Where(b => b.Id == id).FirstOrDefault();

            if (brand != null)
            {
                brand.Cars.ToList().ForEach(c =>
                {
                    var car = db.Cars.Include(a => a.Photos).Include(a => a.Prices).Where(a => a.Id == c.Id).FirstOrDefault();
                    db.Prices.RemoveRange(car.Prices);
                    db.Photos.RemoveRange(car.Photos);
                    db.Cars.Remove(c);
                });
                db.Models.RemoveRange(brand.Models.ToList());
                db.Brands.Remove(brand);
                db.SaveChanges();
            }
        }
    }
}
