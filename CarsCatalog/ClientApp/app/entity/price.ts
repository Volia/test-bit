﻿export class Price {
    constructor(public sum: number, public carId : number, public date: Date = new Date(Date.now()), public id: number = 0) { }
}