﻿using AutoMapper;
using CarsCatalog.BLL.DTO;
using CarsCatalog.BLL.Interfaces;
using CarsCatalog.DAL.Entities;
using CarsCatalog.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarsCatalog.BLL.Services
{
    public class BrandService : IBrandService
    {
        IUnitOfWork Database { get; set; }

        public BrandService(IUnitOfWork uow)
        {
            this.Database = uow;
        }

        public BrandDTO AddBrand(BrandDTO brandDto)
        {
            Brand brand = new Brand { Name = brandDto.Name };
            return Mapper.Map<Brand, BrandDTO>(Database.Brands.Create(brand));
        }

        public BrandDTO GetBrand(int? id)
        {
            var brand = Database.Brands.Get(id.Value);
            return Mapper.Map<Brand, BrandDTO>(brand);
        }

        public IEnumerable<BrandDTO> GetBrands()
        {
            var brands = Database.Brands.GetAll();
            List<BrandDTO> brandDtos = new List<BrandDTO>();
            foreach (var brand in brands)
            {
                brandDtos.Add(Mapper.Map<Brand, BrandDTO>(brand));
            }
            return brandDtos;
        }

        public void Delete(int id)
        {
            Database.Brands.Delete(id);
        }

        public void Update(BrandDTO brandDto)
        {
            Brand brand = Mapper.Map<BrandDTO, Brand>(brandDto);
            Database.Brands.Update(brand);
        }

        public void Dispose()
        {
            Database.Dispose();
        }
    }
}
