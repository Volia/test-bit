﻿import { Brand }  from './brand';
import { Model } from './model';
import { Price } from './price';
import { Photo } from './photo';
export class Car {
    id: number = 0;
    brand: Brand = new Brand();
    model: Model = new Model();
    color: string = '';
    engineCapacity: number = 0;
    prices: Price[] = [];
    photos: Photo[] = [];
    description: string = '';
}