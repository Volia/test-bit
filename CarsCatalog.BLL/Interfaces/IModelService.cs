﻿using CarsCatalog.BLL.DTO;
using System.Collections.Generic;

namespace CarsCatalog.BLL.Interfaces
{
    public interface IModelService
    {
        ModelDTO AddModel(ModelDTO modelDto);
        IEnumerable<ModelDTO> GetModels();
        ModelDTO GetModel(int? id);
        void Update(ModelDTO modelDto);
        void Delete(int id);
        void Dispose();
    }
}
