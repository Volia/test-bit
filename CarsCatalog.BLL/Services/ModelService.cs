﻿using AutoMapper;
using CarsCatalog.BLL.DTO;
using CarsCatalog.BLL.Interfaces;
using CarsCatalog.DAL.Entities;
using CarsCatalog.DAL.Interfaces;
using System.Collections.Generic;

namespace CarsCatalog.BLL.Services
{
    public class ModelService : IModelService
    {
        IUnitOfWork Database { get; set; }

        public ModelService(IUnitOfWork uow)
        {
            this.Database = uow;
        }

        public ModelDTO AddModel(ModelDTO modelDto)
        {
            Model model = Mapper.Map<ModelDTO, Model>(modelDto);
            return Mapper.Map<Model, ModelDTO>(Database.Models.Create(model));
        }

        public ModelDTO GetModel(int? id)
        {
            var model = Database.Models.Get(id.Value);
            return Mapper.Map<Model, ModelDTO>(model);
        }

        public IEnumerable<ModelDTO> GetModels()
        {
            var models = Database.Models.GetAll();
            List<ModelDTO> modelsDtos = new List<ModelDTO>();
            foreach (var model in models)
            {
                modelsDtos.Add(Mapper.Map<Model, ModelDTO>(model));
            }
            return modelsDtos;
        }

        public void Delete(int id)
        {
            Database.Models.Delete(id);
        }

        public void Update(ModelDTO modelDto)
        {
            Model model = Mapper.Map<ModelDTO, Model>(modelDto);
            Database.Models.Update(model);
        }

        public void Dispose()
        {
            Database.Dispose();
        }
    }
}
