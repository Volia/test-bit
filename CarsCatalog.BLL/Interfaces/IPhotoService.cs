﻿using CarsCatalog.BLL.DTO;
using System.Collections.Generic;

namespace CarsCatalog.BLL.Interfaces
{
    public interface IPhotoService
    {
        PhotoDTO AddPhoto(PhotoDTO photoDto);
        void Dispose();
    }
}
