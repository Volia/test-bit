﻿import { Model } from './Model';
export class Brand {
    id: number;
    name: string;
    models: Model[];
    constructor() {
        this.name = '';
    }
}