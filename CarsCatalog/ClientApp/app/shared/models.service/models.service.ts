﻿import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { Model } from '../../entity/model';

@Injectable()
export class ModelsService {
    private apiUrl = 'api/Model';

    constructor(private http: Http) { }

    getModels(): Observable<Model[]> {
        return this.http.get(this.apiUrl)
            .map(res => res.json() as Model[])
            .catch(this.handleError);
    }

    deleteModel(model: Model) {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers });
        let url = `${this.apiUrl}/${model.id}`;
        console.log(model);
        return this.http.delete(url, options).catch(this.handleError);

    }

    createModel(model: Model): Observable<Model> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers });

        return this.http.post(this.apiUrl, JSON.stringify(model), options)
            .map(res => res.json() as Model)
            .catch(this.handleError);
    }

    updateModel(model: Model): Observable<Model> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers });
        let url = `${this.apiUrl}/${model.id}`;
        return this.http.put(url, JSON.stringify(model), options)
            .catch(this.handleError);
    }
    private handleError(error: any) {
        console.error('Error: ', error);
        return Observable.throw(error.message || error);
    }
}