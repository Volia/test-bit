﻿import { Pipe, PipeTransform } from '@angular/core';
import { Car } from '../../entity/car';
import { Price } from '../../entity/price';
import { CarsFilterObject } from '../../entity/filters';
@Pipe({
    name: 'carsFilter', pure: false
})
export class CarsFilterPipe implements PipeTransform {
    transform(items: Car[], filterObj: CarsFilterObject): Car[] {
            if (filterObj.brand != -1) {
                items = items.filter(item => item.brand.id == filterObj.brand);
            }
            if (filterObj.color) {
                items = items.filter(item => item.color == filterObj.color);
            }
            if (filterObj.engineCapacity != -1) {
                items = items.filter(item => item.engineCapacity == filterObj.engineCapacity);
            }
            if (filterObj.model != -1) {
                items = items.filter(item => item.model.id == filterObj.model);
            }
            if (filterObj.date) {
                if (filterObj.minPrice) {
                    items = this.filterItemsByMinPriceWithDate(items, filterObj.minPrice, filterObj.date);
                }
                if (filterObj.maxPrice) {
                    items = this.filterItemsByMaxPriceWithDate(items, filterObj.maxPrice, filterObj.date);
                }
            } else {
                if (filterObj.minPrice) {
                    items = this.filterItemsByMinPrice(items, filterObj.minPrice);
                }
                if (filterObj.maxPrice) {
                    items = this.filterItemsByMaxPrice(items, filterObj.maxPrice);
                }
            }
            return items;
    }

    private filterItemsByMinPrice(items: Car[], minPrice: number): Car[] {
        return items.filter(item => item.prices.sort(((a: Price, b: Price) => {
                            return +new Date(a.date) - +new Date(b.date);
                        }))[item.prices.length - 1].sum >= minPrice);
    }

    private filterItemsByMinPriceWithDate(items: Car[], minPrice: number, date: Date): Car[] {
        let dateSort = new Date(date).toLocaleDateString();
        return items.filter(item =>
            item.prices.filter(price => dateSort == new Date(price.date).toLocaleDateString() && price.sum >= minPrice).length > 0);
    }

    private filterItemsByMaxPrice(items: Car[], maxPrice: number): Car[] {
        return items.filter(item => item.prices.sort(((a: Price, b: Price) => {
            return +new Date(a.date) - +new Date(b.date);
        }))[item.prices.length - 1].sum <= maxPrice);
    }

    private filterItemsByMaxPriceWithDate(items: Car[], maxPrice: number, date: Date): Car[] {
        let dateSort = new Date(date).toLocaleDateString();
        return items.filter(item =>
            item.prices.filter(price => dateSort == new Date(price.date).toLocaleDateString() && price.sum <= maxPrice).length > 0);
    }
}