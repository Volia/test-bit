﻿import { Component, OnInit } from '@angular/core';
import { BrandsService } from '../../shared/brands.service/brands.service';
import { ModelsService } from '../../shared/models.service/models.service';

import { Brand } from '../../entity/brand';
import { Model } from '../../entity/model';
@Component({
    selector: 'model',
    templateUrl: './model.component.html',
    styleUrls: ['./model.component.css'],
    providers: [BrandsService]
})
export class ModelComponent implements OnInit {
    brands: Brand[];
    model: Model;
    brand: Brand;
    constructor(private brandService: BrandsService, private modelsService: ModelsService) {
        this.brands = [];
        this.model = new Model();
    }

    ngOnInit() {
        this.brandService.getBrands().subscribe(brands => {
            this.brands = brands;
        });
    }
    onSelectBrand(brand: Brand): void {
        this.model = new Model();
        this.brand = brand;
    };
    onSelectModel(model: Model): void {
        this.model = model;
    }
    onDelete(model: Model): void {
        this.modelsService.deleteModel(model).subscribe(res => {
            let index = this.brands.filter(b => b.id == model.brandId)[0].models.indexOf(model);

            if (index > -1) {
                this.brands.filter(b => b.id == model.brandId)[0].models.splice(index, 1);
            }
        })
    }

    onCreate(model: Model) {
        model.brandId = this.brand.id;
        let modelName = model.name;
        if (!model.id) {
            this.modelsService.createModel(model).subscribe(res => {
                this.brands.filter(b => b.id == res.brandId)[0].models.push(res);
            });
        } else {
            this.modelsService.updateModel(model).subscribe(res => {
                this.brands.filter(b => b.id == model.brandId)[0].models.filter(m => m.id == model.id)[0].name = modelName;
            });
        }
    }
}
