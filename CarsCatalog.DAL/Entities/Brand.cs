﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CarsCatalog.DAL.Entities
{
    public class Brand
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Model> Models { get; set; }
        public virtual ICollection<Car> Cars { get; set; }
    }
}