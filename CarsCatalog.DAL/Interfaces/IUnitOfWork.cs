﻿using CarsCatalog.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarsCatalog.DAL.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<Brand> Brands { get; }
        IRepository<Price> Prices { get; }
        IRepository<Model> Models { get; }
        IRepository<Photo> Photos { get; }
        IRepository<Car> Cars { get; }
        void Save();
    }
}
