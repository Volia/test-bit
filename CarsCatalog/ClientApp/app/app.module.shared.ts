import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './components/app/app.component';
import { NavMenuComponent } from './components/navmenu/navmenu.component';
import { HomeComponent } from './components/home/home.component';
import { CarViewComponent } from './components/carview/carview.component';
import { FilterComponent } from './components/filter/filter.component';
import { BrandtreeComponent } from './components/brandtree/brandree.component';
import { AutocompleteComponent } from './components/autocomplete/autocomplete.component';
import { BrandComponent } from './components/brand/brand.component';
import { AddBrandComponent } from './components/addbrand/addbrand.component';
import { ModelComponent } from './components/model/Model.component';
import { AddModelComponent } from './components/addmodel/addmodel.component';
import { CarphotosComponent } from './components/car/carphotos/carphotos.component';
import { CarpageComponent } from './components/car/carpage/carpage.component';
import { ViewinfoComponent } from './components/car/viewinfo/viewinfo.component';
import { EditinfoComponent } from './components/car/editinfo/editinfo.component';
import { CarsFilterPipe } from './shared/filters/cars.pipe';
import { ImageUploadModule } from "angular2-image-upload";

@NgModule({
    declarations: [
        AppComponent,
        NavMenuComponent,
        CarViewComponent,
        HomeComponent,
        FilterComponent,
        BrandtreeComponent,
        AutocompleteComponent,
        BrandComponent,
        AddBrandComponent,
        ModelComponent,
        AddModelComponent,
        CarphotosComponent,
        CarpageComponent,
        ViewinfoComponent,
        EditinfoComponent,
        CarsFilterPipe
    ],
    imports: [
        CommonModule,
        HttpModule,
        FormsModule,
        ImageUploadModule.forRoot(),
        RouterModule.forRoot([
            { path: '', redirectTo: 'home', pathMatch: 'full' },
            { path: 'home', component: HomeComponent, pathMatch: 'full' },
            { path: 'brand', component: BrandComponent, pathMatch: 'full' },
            { path: 'model', component: ModelComponent, pathMatch: 'full' },
            { path: 'car/:mode/:id', component: CarpageComponent },
            { path: '**', redirectTo: 'home' }
        ])
    ]
})
export class AppModuleShared {
}
