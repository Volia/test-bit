import { Component, OnInit } from '@angular/core';
import { Car } from '../../../entity/car';
import { Brand } from '../../../entity/brand';
import { Model } from '../../../entity/model';
import { Price } from '../../../entity/price';
import { CarsService } from '../../../shared/cars.service/cars.service';
import { BrandsService } from '../../../shared/brands.service/brands.service';
import { PricesService } from '../../../shared/prices.service/prices.service';
import { ActivatedRoute } from "@angular/router";
@Component({
    selector: 'car-page',
    templateUrl: './carpage.component.html',
    styleUrls: ['./carpage.component.css'],
    providers: [CarsService, BrandsService, PricesService]
})
export class CarpageComponent implements OnInit {
    car: Car;
    id: number;
    mode: string;
    brands: Brand[];
    constructor(
        private carsService: CarsService,
        private brandsService: BrandsService,
        private pricesService: PricesService,
        private route: ActivatedRoute) {
        this.route.params.subscribe(params => {
            this.id = params.id;
            this.mode = params.mode;
        });
        this.brands = [];
        if (this.mode == 'create') {
            this.car = new Car();
        }
    }

    ngOnInit() {
        if (this.mode != 'create') {
            this.carsService.getCar(this.id).subscribe(car => {
                this.car = car;
            });
        }
        this.brandsService.getBrands().subscribe(brands => {
            this.brands = brands;
        });
    }

    onSelectBrand(brand: Brand) {
        this.car.brand = brand;
        this.car.model = new Model();
    }

    onSelectModel(model: Model) {
        this.car.model = model;
    }

    onEditCar(data: any) {
        let current: number = data.current;
        let last: number = data.last;
        this.car = data.car;
        if (this.mode != 'create') {
            if (current != last) {
                this.pricesService.createPrice(new Price(current, this.car.id)).subscribe(price => {
                    this.car.prices.push(price);
                });
            }
            this.carsService.updateCar(this.car).subscribe();
        } else {
            this.car.prices.push(new Price(current, this.car.id));
            this.carsService.createCar(this.car).subscribe(car => this.car = car);
        }
    }
}
