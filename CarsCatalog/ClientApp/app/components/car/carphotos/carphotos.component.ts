import { Component, Input } from '@angular/core';

import { Photo } from '../../../entity/photo';
@Component({
    selector: 'car-photos',
    templateUrl: './carphotos.component.html',
    styleUrls: ['./carphotos.component.css']
})
export class CarphotosComponent {
    @Input() photos: Photo[];
    photoUrl: string;
    constructor() {
        this.photoUrl = 'assets/photos/';
    }
}
