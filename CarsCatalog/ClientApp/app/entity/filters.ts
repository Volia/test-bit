﻿export class CarsFilterObject {
    public model: number;
    public brand: number;
    public color: string;
    public engineCapacity: number;
    public minPrice: number;
    public maxPrice: number;
    public date: Date;
    constructor() {
        this.brand = -1;
        this.color = '';
        this.engineCapacity = -1;
        this.model = -1;
        this.maxPrice = 0;
        this.minPrice = 0;
    }
}