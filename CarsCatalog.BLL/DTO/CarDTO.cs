﻿using System.Collections.Generic;

namespace CarsCatalog.BLL.DTO
{
    public class CarDTO
    {
        public int Id { get; set; }
        /*public int ModelId { get; set; }*/
        public ModelDTO Model { get; set; }
        public BrandDTO Brand { get; set; }
        public string Color { get; set; }
        public float EngineCapacity { get; set; }
        public string Description { get; set; }

        public ICollection<PriceDTO> Prices { get; set; }
        public ICollection<PhotoDTO> Photos { get; set; }
    }
}
